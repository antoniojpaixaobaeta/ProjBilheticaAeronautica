﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiCliente.Controllers
{
    public class ReservasController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: api/Reservas
        public List<Reserva> Get()
        {
            var lista = from Reserva in dc.Reservas select Reserva;

            return lista.ToList();
        }

        // GET: api/Reservas/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Reservas
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Reservas/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Reservas/5
        public void Delete(int id)
        {
        }
    }
}
