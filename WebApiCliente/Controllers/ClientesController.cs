﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiCliente.Controllers
{
    public class ClientesController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        // GET: api/Clientes
        public List<Cliente> Get()
        {
            var lista = from Cliente in dc.Clientes select Cliente;

            return lista.ToList();
        }

        // GET: api/Clientes/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Clientes
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Clientes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Clientes/5
        public void Delete(int id)
        {
        }
    }
}
