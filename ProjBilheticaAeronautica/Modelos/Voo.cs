﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Modelos
{
    public partial class Voo
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();

        public void Create(int idvoo, int idaparelho, decimal precoeconomica, decimal precoexecutiva, string origem, string destino, DateTime data, string hora, int executivadisponiveis, int economicadisponiveis)
        {
            Voo novoVoo = new Voo
            {
                IDVoo = idvoo,
                IDAparelho = idaparelho,
                PrecoEconomica = precoeconomica,
                PrecoExecutiva = precoexecutiva, 
                Origem = origem,
                Destino = destino,
                Data = data,
                Hora = TimeSpan.Parse(hora), 
                ExecutivaDisponiveis = executivadisponiveis,
                EconomicaDisponiveis = economicadisponiveis,



            };

            db.Voos.InsertOnSubmit(novoVoo);

            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

        }

        public Voo Read(int id)
        {
            Voo procuraVoo = new Voo();

            var pesquisa = from Voo in db.Voos
                           where Voo.IDVoo == id
                           select Voo;

            procuraVoo = pesquisa.Single();

            return procuraVoo;
        }

        public void Update(int idvoo, decimal precoeconomica, decimal precoexecutiva, string origem, string destino, DateTime data, string hora)
        {
            Voo updateVoo = new Voo();

            var pesquisa = from Voo in db.Voos
                           where Voo.IDVoo == idvoo
                           select Voo;

            updateVoo = pesquisa.Single();

            updateVoo.PrecoEconomica = precoeconomica;
            updateVoo.PrecoExecutiva = precoexecutiva;
            updateVoo.Origem = origem;
            updateVoo.Destino = destino;
            updateVoo.Data = data;
            updateVoo.Hora = TimeSpan.Parse(hora);


            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }

        public bool Delete(int id)
        {
            bool ex = false;

            Voo apagarVoo = new Voo();

            var pesquisa = from Voo in db.Voos
                           where Voo.IDVoo == id
                           select Voo;

            apagarVoo = pesquisa.Single();

            db.Voos.DeleteOnSubmit(apagarVoo);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possível apagar o voo selecionado. Verifique os seus registos");

                ex = true;
            }
            return ex;
        }

        public int GeraID()

        {
            int id = 0;

            Voo ultimoID = new Voo();

            var pesquisa = (from Voo in db.Voos
                            orderby Voo.IDVoo descending
                            select Voo.IDVoo).First();

            return id = pesquisa + 1;
        }
    }
}
