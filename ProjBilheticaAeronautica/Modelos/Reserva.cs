﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Modelos
{
    public partial class Reserva
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();

        public void Create(int idreserva, int idcliente, int idaparelho, int idvoo, int economica, int executiva)
        {
            int total = SomaBilhetes(idvoo, economica, executiva);

            Reserva novaReserva = new Reserva
            {
                IDReserva = idreserva,
                IDCliente = idcliente,
                IDAparelho =idaparelho,
                IDVoo = idvoo,
                Total = total,
                Economica = economica,
                Executiva = executiva,
        

            };

            db.Reservas.InsertOnSubmit(novaReserva);

            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

        }

        public Reserva Read(int id)
        {
            Reserva procuraReserva = new Reserva();

            var pesquisa = from Reserva in db.Reservas
                           where Reserva.IDReserva == id
                           select Reserva;

            procuraReserva = pesquisa.Single();

            return procuraReserva;
        }

        public void Update(int idreserva, int idcliente, int idvoo, string idlugar)
        {
            Reserva updateReserva = new Reserva
            {
                IDReserva = idreserva,
                IDCliente = idcliente,
                IDVoo = idvoo,
            };

            db.Reservas.InsertOnSubmit(updateReserva);

            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }

        public void Delete(int id)
        {
            Reserva apagarReserva = new Reserva();

            var pesquisa = from Reserva in db.Reservas
                           where Reserva.IDReserva == id
                           select Reserva;

            apagarReserva = pesquisa.Single();

            db.Reservas.DeleteOnSubmit(apagarReserva);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public int SomaBilhetes(int id, int economica, int executiva)
        {
            Voo procuraVoo = new Voo();

            var pesquisa = from Voo in db.Voos
                            where Voo.IDVoo == id
                            select Voo;

            procuraVoo = pesquisa.Single();

            int PrecoEco = (decimal.ToInt32(procuraVoo.PrecoEconomica));
            int PrecoExec = (decimal.ToInt32(procuraVoo.PrecoExecutiva));

            procuraVoo.ExecutivaDisponiveis -= executiva;
            procuraVoo.EconomicaDisponiveis -= economica;

            int Total = (economica * PrecoEco) + (executiva * PrecoExec);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return Total;
        }

        public int GeraID()

        {
            int id = 0;

            var pesquisa = (from Reserva in db.Reservas
                            orderby Reserva.IDReserva descending
                            select Reserva.IDReserva).First();

            return id = pesquisa + 1;
        }

    }
}

