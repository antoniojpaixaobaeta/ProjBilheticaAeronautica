﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Modelos
{
    public partial class Cliente
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();
        
        public void Create(int idcliente, string nomeempresa, int telefone, int nif, string email, string username, string password)
        {
            Cliente novoCliente = new Cliente
            {
                IDCliente = idcliente,
                NomeEmpresa = nomeempresa,
                Telefone = telefone,
                Nif = nif,
                Email = email,
                Username = username,
                Password = password,
            };

            db.Clientes.InsertOnSubmit(novoCliente);

            try
            {
                db.SubmitChanges();
           
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
            
        }
        

        public Cliente Read(int id)
        {
            Cliente procuraCliente = new Cliente();

            var pesquisa = from Cliente in db.Clientes
                           where Cliente.IDCliente == id
                           select Cliente;

            procuraCliente = pesquisa.Single();

            return procuraCliente;
        }

        public void Update(int id, string nomeempresa, int telefone, int nif, string email, string username, string password)
        {
            Cliente updateCliente = new Cliente();
            
            var pesquisa = from Cliente in db.Clientes
                          where Cliente.IDCliente == id
                          select Cliente;

            updateCliente = pesquisa.Single();

            updateCliente.NomeEmpresa = nomeempresa;
            updateCliente.Telefone = telefone;
            updateCliente.Email = email;
            updateCliente.Username = username;
            updateCliente.Password = password;


            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }

        public bool Delete(int id)
        {
            bool ex = false;

            Cliente apagarCliente = new Cliente();

            var pesquisa = from Cliente in db.Clientes
                           where Cliente.IDCliente == id
                           select Cliente;

            apagarCliente = pesquisa.Single();

            db.Clientes.DeleteOnSubmit(apagarCliente);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possível apagar o cliente. Verifique os seus registos");

                ex = true;
            }
            return ex;
        }
        
        public int GeraID()

        {
            int id = 0;

            Cliente ultimoID = new Cliente();

            var pesquisa = (from Cliente in db.Clientes
                           orderby Cliente.IDCliente descending
                           select Cliente.IDCliente).First();

            return id = pesquisa + 1;
        }

        
    }
}
