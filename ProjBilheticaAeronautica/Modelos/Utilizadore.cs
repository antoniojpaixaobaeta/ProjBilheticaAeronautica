﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Modelos
{
    public partial class Utilizadore
    {

        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();

        public bool Login(string username, string password)
        {
            bool ver = false;

            Utilizadore encontraUsername = new Utilizadore();

            var pesquisa = from Utilizadore in db.Utilizadores
                           where Utilizadore.Username == username
                           select Utilizadore;

            encontraUsername = pesquisa.SingleOrDefault();

            if (encontraUsername == null)
            {
                MessageBox.Show("Password ou username errado!");

                return ver = false;
            }

            if (encontraUsername.Password == password)
            {
                MessageBox.Show("Login efectuado com sucesso! Bem-vindo " + username + "!");

                return ver = true;
            }

            else
            {
                MessageBox.Show("Password ou username errado!");

                return ver = false;
            }
        }

    }

}

