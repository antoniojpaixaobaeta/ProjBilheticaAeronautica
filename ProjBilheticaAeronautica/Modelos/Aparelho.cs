﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Modelos
{
    public partial class Aparelho
    {

        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();

        public void Create(int idaparelho, string nomeaparelho, int lugareseconomica, int lugaresexecutiva)
        {
            Aparelho novoAparelho = new Aparelho
            {
                IDAparelho = idaparelho,
                NomeAparelho = nomeaparelho,
                LugaresEconomica = lugareseconomica,
                LugaresExecutiva = lugaresexecutiva,
            };

            db.Aparelhos.InsertOnSubmit(novoAparelho);

            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

        }

        internal int GeraID()
        {
            int id = 0;

            var pesquisa = (from Aparelho in db.Aparelhos
                            orderby Aparelho.IDAparelho descending
                            select Aparelho.IDAparelho).First();

            return id = pesquisa + 1;
        }

        public Aparelho Read(int id)
        {
            Aparelho procuraAparelho = new Aparelho();

            var pesquisa = from Aparelho in db.Aparelhos
                           where Aparelho.IDAparelho == id
                           select Aparelho;

            procuraAparelho = pesquisa.Single();

            return procuraAparelho;
        }

        public void Update(int idaparelho, string nomeaparelho, int lugareseconomica, int lugaresexecutiva)
        {
            Aparelho updateAparelho = new Aparelho
            {
                IDAparelho = idaparelho,
                NomeAparelho = nomeaparelho,
                LugaresEconomica = lugareseconomica,
                LugaresExecutiva = lugaresexecutiva,
            };

            db.Aparelhos.InsertOnSubmit(updateAparelho);

            try
            {
                db.SubmitChanges();

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
        }

        public void Delete(int id)
        {
            Aparelho apagarAparelho = new Aparelho();

            var pesquisa = from Aparelho in db.Aparelhos
                           where Aparelho.IDAparelho == id
                           select Aparelho;

            apagarAparelho = pesquisa.Single();

            db.Aparelhos.DeleteOnSubmit(apagarAparelho);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

