﻿using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Forms
{
    public partial class ClientesMainForm : MetroFramework.Forms.MetroForm
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();
        Cliente cliente = new Cliente();
        ClientesNovo clientesNovo = new ClientesNovo();

        public ClientesMainForm()
        {
            InitializeComponent();
            LoadComboID();
            LoadLabels();
        }
        /// <summary>
        /// Método que faz load das labels que mostram os detalhes sobre os clientes
        /// </summary>
        private void LoadLabels()
        {
            cliente = new Cliente();

            cliente = cliente.Read(Convert.ToInt32(comboID.Text));

            lblShowNome.Text = cliente.NomeEmpresa;
            lblShowTelefone.Text = cliente.Telefone.ToString();
            lblShowNif.Text = cliente.Nif.ToString();
            lblShowEmail.Text = cliente.Email;
            lblShowUsername.Text = cliente.Username;
            lblShowPass.Text = cliente.Password;

        }

        /// <summary>
        /// Método que faz load da combobox que ajuda a procurar pelo ID
        /// </summary>
        private void LoadComboID()
        {

            foreach (var item in db.Clientes)
            {
                comboID.Items.Add(item.IDCliente);
            }

            var pesquisa = (from Cliente in db.Clientes
                            orderby Cliente.IDCliente ascending
                            select Cliente.IDCliente).First();

            comboID.Text = pesquisa.ToString();
        }

        private void comboID_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLabels();
        }

        private void bttnApagar_Click(object sender, EventArgs e)
        {
            

            bool ex = cliente.Delete(Convert.ToInt32(comboID.Text));
            
            if (ex == false)
            {
                MessageBox.Show("Cliente apagado com sucesso!");
                comboID.Items.Clear();
                LoadComboID();
            }
            else
            {
                return;
            }
            
        }
        private void bttnEditar_Click(object sender, EventArgs e)
        {
            int nif;
            int telefone;
            

            if (!((string.IsNullOrEmpty(lblShowNome.Text)) || ((string.IsNullOrEmpty(lblShowTelefone.Text)) || ((string.IsNullOrEmpty(lblShowNif.Text)) ||
                (string.IsNullOrEmpty(lblShowEmail.Text)) || (string.IsNullOrEmpty(lblShowUsername.Text)) || (string.IsNullOrEmpty(lblShowPass.Text))))))
            {
                if (int.TryParse(lblShowTelefone.Text, out telefone) && (int.TryParse(lblShowNif.Text, out nif)))

                {
                    cliente.Update(Convert.ToInt32(comboID.Text), lblShowNome.Text, Convert.ToInt32(lblShowTelefone.Text), Convert.ToInt32(lblShowNif.Text), lblShowEmail.Text, lblShowUsername.Text, lblShowPass.Text);

                    MessageBox.Show("Cliente editado com sucesso!");

                }

                else
                {
                    MessageBox.Show("Campo numérico inválido. Por favor insira somente números");
                }
            }
            else
            {
                MessageBox.Show("Não pode deixar nenhum campo em branco. Por favor preencha todos os campos.");
            }
        }

        private void bttnNovo_Click(object sender, EventArgs e)
        {
            Hide();
            clientesNovo = new ClientesNovo();
            clientesNovo.Show();
            
        }
    }
}
