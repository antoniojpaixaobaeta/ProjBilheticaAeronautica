﻿namespace ProjBilheticaAeronautica.Forms
{
    partial class ClientesMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.comboID = new MetroFramework.Controls.MetroComboBox();
            this.bttnNovo = new MetroFramework.Controls.MetroButton();
            this.bttnEditar = new MetroFramework.Controls.MetroButton();
            this.bttnApagar = new MetroFramework.Controls.MetroButton();
            this.lblShowNome = new MetroFramework.Controls.MetroTextBox();
            this.lblShowTelefone = new MetroFramework.Controls.MetroTextBox();
            this.lblShowNif = new MetroFramework.Controls.MetroTextBox();
            this.lblShowEmail = new MetroFramework.Controls.MetroTextBox();
            this.lblShowUsername = new MetroFramework.Controls.MetroTextBox();
            this.lblShowPass = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(32, 103);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(120, 19);
            this.metroLabel1.TabIndex = 38;
            this.metroLabel1.Text = "Nome da Empresa";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(308, 143);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(68, 19);
            this.metroLabel3.TabIndex = 40;
            this.metroLabel3.Text = "Username";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(32, 184);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(29, 19);
            this.metroLabel4.TabIndex = 41;
            this.metroLabel4.Text = "NIF";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(308, 188);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(63, 19);
            this.metroLabel6.TabIndex = 43;
            this.metroLabel6.Text = "Password";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(308, 103);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(41, 19);
            this.metroLabel9.TabIndex = 46;
            this.metroLabel9.Text = "Email";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(32, 143);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(57, 19);
            this.metroLabel10.TabIndex = 47;
            this.metroLabel10.Text = "Telefone";
            // 
            // comboID
            // 
            this.comboID.FormattingEnabled = true;
            this.comboID.ItemHeight = 23;
            this.comboID.Location = new System.Drawing.Point(48, 252);
            this.comboID.Name = "comboID";
            this.comboID.Size = new System.Drawing.Size(481, 29);
            this.comboID.TabIndex = 50;
            this.comboID.UseSelectable = true;
            this.comboID.SelectedIndexChanged += new System.EventHandler(this.comboID_SelectedIndexChanged);
            // 
            // bttnNovo
            // 
            this.bttnNovo.Location = new System.Drawing.Point(149, 347);
            this.bttnNovo.Name = "bttnNovo";
            this.bttnNovo.Size = new System.Drawing.Size(97, 78);
            this.bttnNovo.TabIndex = 51;
            this.bttnNovo.Text = "Novo";
            this.bttnNovo.UseSelectable = true;
            this.bttnNovo.Click += new System.EventHandler(this.bttnNovo_Click);
            // 
            // bttnEditar
            // 
            this.bttnEditar.Location = new System.Drawing.Point(243, 347);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(97, 78);
            this.bttnEditar.TabIndex = 52;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.UseSelectable = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // bttnApagar
            // 
            this.bttnApagar.Location = new System.Drawing.Point(337, 347);
            this.bttnApagar.Name = "bttnApagar";
            this.bttnApagar.Size = new System.Drawing.Size(97, 78);
            this.bttnApagar.TabIndex = 53;
            this.bttnApagar.Text = "Apagar";
            this.bttnApagar.UseSelectable = true;
            this.bttnApagar.Click += new System.EventHandler(this.bttnApagar_Click);
            // 
            // lblShowNome
            // 
            // 
            // 
            // 
            this.lblShowNome.CustomButton.Image = null;
            this.lblShowNome.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowNome.CustomButton.Name = "";
            this.lblShowNome.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowNome.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowNome.CustomButton.TabIndex = 1;
            this.lblShowNome.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowNome.CustomButton.UseSelectable = true;
            this.lblShowNome.CustomButton.Visible = false;
            this.lblShowNome.Lines = new string[0];
            this.lblShowNome.Location = new System.Drawing.Point(158, 103);
            this.lblShowNome.MaxLength = 32767;
            this.lblShowNome.Name = "lblShowNome";
            this.lblShowNome.PasswordChar = '\0';
            this.lblShowNome.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowNome.SelectedText = "";
            this.lblShowNome.SelectionLength = 0;
            this.lblShowNome.SelectionStart = 0;
            this.lblShowNome.ShortcutsEnabled = true;
            this.lblShowNome.Size = new System.Drawing.Size(96, 23);
            this.lblShowNome.TabIndex = 54;
            this.lblShowNome.UseSelectable = true;
            this.lblShowNome.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowNome.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowTelefone
            // 
            // 
            // 
            // 
            this.lblShowTelefone.CustomButton.Image = null;
            this.lblShowTelefone.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowTelefone.CustomButton.Name = "";
            this.lblShowTelefone.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowTelefone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowTelefone.CustomButton.TabIndex = 1;
            this.lblShowTelefone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowTelefone.CustomButton.UseSelectable = true;
            this.lblShowTelefone.CustomButton.Visible = false;
            this.lblShowTelefone.Lines = new string[0];
            this.lblShowTelefone.Location = new System.Drawing.Point(158, 143);
            this.lblShowTelefone.MaxLength = 32767;
            this.lblShowTelefone.Name = "lblShowTelefone";
            this.lblShowTelefone.PasswordChar = '\0';
            this.lblShowTelefone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowTelefone.SelectedText = "";
            this.lblShowTelefone.SelectionLength = 0;
            this.lblShowTelefone.SelectionStart = 0;
            this.lblShowTelefone.ShortcutsEnabled = true;
            this.lblShowTelefone.Size = new System.Drawing.Size(96, 23);
            this.lblShowTelefone.TabIndex = 55;
            this.lblShowTelefone.UseSelectable = true;
            this.lblShowTelefone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowTelefone.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowNif
            // 
            // 
            // 
            // 
            this.lblShowNif.CustomButton.Image = null;
            this.lblShowNif.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowNif.CustomButton.Name = "";
            this.lblShowNif.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowNif.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowNif.CustomButton.TabIndex = 1;
            this.lblShowNif.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowNif.CustomButton.UseSelectable = true;
            this.lblShowNif.CustomButton.Visible = false;
            this.lblShowNif.Lines = new string[0];
            this.lblShowNif.Location = new System.Drawing.Point(158, 184);
            this.lblShowNif.MaxLength = 32767;
            this.lblShowNif.Name = "lblShowNif";
            this.lblShowNif.PasswordChar = '\0';
            this.lblShowNif.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowNif.SelectedText = "";
            this.lblShowNif.SelectionLength = 0;
            this.lblShowNif.SelectionStart = 0;
            this.lblShowNif.ShortcutsEnabled = true;
            this.lblShowNif.Size = new System.Drawing.Size(96, 23);
            this.lblShowNif.TabIndex = 56;
            this.lblShowNif.UseSelectable = true;
            this.lblShowNif.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowNif.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowEmail
            // 
            // 
            // 
            // 
            this.lblShowEmail.CustomButton.Image = null;
            this.lblShowEmail.CustomButton.Location = new System.Drawing.Point(101, 1);
            this.lblShowEmail.CustomButton.Name = "";
            this.lblShowEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowEmail.CustomButton.TabIndex = 1;
            this.lblShowEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowEmail.CustomButton.UseSelectable = true;
            this.lblShowEmail.CustomButton.Visible = false;
            this.lblShowEmail.Lines = new string[0];
            this.lblShowEmail.Location = new System.Drawing.Point(355, 103);
            this.lblShowEmail.MaxLength = 32767;
            this.lblShowEmail.Name = "lblShowEmail";
            this.lblShowEmail.PasswordChar = '\0';
            this.lblShowEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowEmail.SelectedText = "";
            this.lblShowEmail.SelectionLength = 0;
            this.lblShowEmail.SelectionStart = 0;
            this.lblShowEmail.ShortcutsEnabled = true;
            this.lblShowEmail.Size = new System.Drawing.Size(123, 23);
            this.lblShowEmail.TabIndex = 57;
            this.lblShowEmail.UseSelectable = true;
            this.lblShowEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowUsername
            // 
            // 
            // 
            // 
            this.lblShowUsername.CustomButton.Image = null;
            this.lblShowUsername.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowUsername.CustomButton.Name = "";
            this.lblShowUsername.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowUsername.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowUsername.CustomButton.TabIndex = 1;
            this.lblShowUsername.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowUsername.CustomButton.UseSelectable = true;
            this.lblShowUsername.CustomButton.Visible = false;
            this.lblShowUsername.Lines = new string[0];
            this.lblShowUsername.Location = new System.Drawing.Point(382, 139);
            this.lblShowUsername.MaxLength = 32767;
            this.lblShowUsername.Name = "lblShowUsername";
            this.lblShowUsername.PasswordChar = '\0';
            this.lblShowUsername.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowUsername.SelectedText = "";
            this.lblShowUsername.SelectionLength = 0;
            this.lblShowUsername.SelectionStart = 0;
            this.lblShowUsername.ShortcutsEnabled = true;
            this.lblShowUsername.Size = new System.Drawing.Size(96, 23);
            this.lblShowUsername.TabIndex = 58;
            this.lblShowUsername.UseSelectable = true;
            this.lblShowUsername.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowUsername.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowPass
            // 
            // 
            // 
            // 
            this.lblShowPass.CustomButton.Image = null;
            this.lblShowPass.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowPass.CustomButton.Name = "";
            this.lblShowPass.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowPass.CustomButton.TabIndex = 1;
            this.lblShowPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowPass.CustomButton.UseSelectable = true;
            this.lblShowPass.CustomButton.Visible = false;
            this.lblShowPass.Lines = new string[0];
            this.lblShowPass.Location = new System.Drawing.Point(382, 188);
            this.lblShowPass.MaxLength = 32767;
            this.lblShowPass.Name = "lblShowPass";
            this.lblShowPass.PasswordChar = '\0';
            this.lblShowPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowPass.SelectedText = "";
            this.lblShowPass.SelectionLength = 0;
            this.lblShowPass.SelectionStart = 0;
            this.lblShowPass.ShortcutsEnabled = true;
            this.lblShowPass.Size = new System.Drawing.Size(96, 23);
            this.lblShowPass.TabIndex = 59;
            this.lblShowPass.UseSelectable = true;
            this.lblShowPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // ClientesMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.lblShowPass);
            this.Controls.Add(this.lblShowUsername);
            this.Controls.Add(this.lblShowEmail);
            this.Controls.Add(this.lblShowNif);
            this.Controls.Add(this.lblShowTelefone);
            this.Controls.Add(this.lblShowNome);
            this.Controls.Add(this.bttnApagar);
            this.Controls.Add(this.bttnEditar);
            this.Controls.Add(this.bttnNovo);
            this.Controls.Add(this.comboID);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel1);
            this.Name = "ClientesMainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox comboID;
        private MetroFramework.Controls.MetroButton bttnNovo;
        private MetroFramework.Controls.MetroButton bttnEditar;
        private MetroFramework.Controls.MetroButton bttnApagar;
        private MetroFramework.Controls.MetroTextBox lblShowNome;
        private MetroFramework.Controls.MetroTextBox lblShowTelefone;
        private MetroFramework.Controls.MetroTextBox lblShowNif;
        private MetroFramework.Controls.MetroTextBox lblShowEmail;
        private MetroFramework.Controls.MetroTextBox lblShowUsername;
        private MetroFramework.Controls.MetroTextBox lblShowPass;
    }
}