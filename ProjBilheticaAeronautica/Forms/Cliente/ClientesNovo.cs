﻿using ProjBilheticaAeronautica.Forms;
using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica
{
    public partial class ClientesNovo : MetroFramework.Forms.MetroForm
    {
        Cliente cliente = new Cliente();
        public ClientesNovo()
        {
            InitializeComponent();
        }

        private void bttnGravar_Click(object sender, EventArgs e)
        {
            int id = cliente.GeraID();
            int nif;
            int telefone;

            if (!((string.IsNullOrEmpty(txtNome.Text)) || ((string.IsNullOrEmpty(txtTelefone.Text)) || ((string.IsNullOrEmpty(txtNif.Text)) ||
                (string.IsNullOrEmpty(txtEmail.Text)) || (string.IsNullOrEmpty(txtUsername.Text)) || (string.IsNullOrEmpty(txtPassword.Text))))))
            {
                if (int.TryParse(txtTelefone.Text, out telefone) && (int.TryParse(txtNif.Text, out nif)))

                {
                    cliente.Create(id, txtNome.Text, Convert.ToInt32(txtTelefone.Text), Convert.ToInt32(txtNif.Text), txtEmail.Text, txtUsername.Text, txtPassword.Text);

                    MessageBox.Show("Cliente criado com sucesso!");

                    Hide();

                }

                else
                {
                    MessageBox.Show("Campo numérico inválido. Por favor insira somente números");
                }
            }
            else
            {
                MessageBox.Show("Não pode deixar nenhum campo em branco. Por favor preencha todos os campos.");

                return;
            }
        }
    }
}
