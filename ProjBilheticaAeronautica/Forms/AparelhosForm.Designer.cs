﻿namespace ProjBilheticaAeronautica
{
    partial class AparelhosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNomeAparelho = new MetroFramework.Controls.MetroTextBox();
            this.txtLugaresExec = new MetroFramework.Controls.MetroTextBox();
            this.txtLugaresEc = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.bttnGravar = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // txtNomeAparelho
            // 
            // 
            // 
            // 
            this.txtNomeAparelho.CustomButton.Image = null;
            this.txtNomeAparelho.CustomButton.Location = new System.Drawing.Point(209, 1);
            this.txtNomeAparelho.CustomButton.Name = "";
            this.txtNomeAparelho.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNomeAparelho.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNomeAparelho.CustomButton.TabIndex = 1;
            this.txtNomeAparelho.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNomeAparelho.CustomButton.UseSelectable = true;
            this.txtNomeAparelho.CustomButton.Visible = false;
            this.txtNomeAparelho.Lines = new string[0];
            this.txtNomeAparelho.Location = new System.Drawing.Point(243, 148);
            this.txtNomeAparelho.MaxLength = 32767;
            this.txtNomeAparelho.Name = "txtNomeAparelho";
            this.txtNomeAparelho.PasswordChar = '\0';
            this.txtNomeAparelho.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNomeAparelho.SelectedText = "";
            this.txtNomeAparelho.SelectionLength = 0;
            this.txtNomeAparelho.SelectionStart = 0;
            this.txtNomeAparelho.ShortcutsEnabled = true;
            this.txtNomeAparelho.Size = new System.Drawing.Size(231, 23);
            this.txtNomeAparelho.TabIndex = 2;
            this.txtNomeAparelho.UseSelectable = true;
            this.txtNomeAparelho.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNomeAparelho.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLugaresExec
            // 
            // 
            // 
            // 
            this.txtLugaresExec.CustomButton.Image = null;
            this.txtLugaresExec.CustomButton.Location = new System.Drawing.Point(209, 1);
            this.txtLugaresExec.CustomButton.Name = "";
            this.txtLugaresExec.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLugaresExec.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLugaresExec.CustomButton.TabIndex = 1;
            this.txtLugaresExec.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLugaresExec.CustomButton.UseSelectable = true;
            this.txtLugaresExec.CustomButton.Visible = false;
            this.txtLugaresExec.Lines = new string[0];
            this.txtLugaresExec.Location = new System.Drawing.Point(243, 225);
            this.txtLugaresExec.MaxLength = 32767;
            this.txtLugaresExec.Name = "txtLugaresExec";
            this.txtLugaresExec.PasswordChar = '\0';
            this.txtLugaresExec.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLugaresExec.SelectedText = "";
            this.txtLugaresExec.SelectionLength = 0;
            this.txtLugaresExec.SelectionStart = 0;
            this.txtLugaresExec.ShortcutsEnabled = true;
            this.txtLugaresExec.Size = new System.Drawing.Size(231, 23);
            this.txtLugaresExec.TabIndex = 4;
            this.txtLugaresExec.UseSelectable = true;
            this.txtLugaresExec.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLugaresExec.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLugaresEc
            // 
            // 
            // 
            // 
            this.txtLugaresEc.CustomButton.Image = null;
            this.txtLugaresEc.CustomButton.Location = new System.Drawing.Point(209, 1);
            this.txtLugaresEc.CustomButton.Name = "";
            this.txtLugaresEc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLugaresEc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLugaresEc.CustomButton.TabIndex = 1;
            this.txtLugaresEc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLugaresEc.CustomButton.UseSelectable = true;
            this.txtLugaresEc.CustomButton.Visible = false;
            this.txtLugaresEc.Lines = new string[0];
            this.txtLugaresEc.Location = new System.Drawing.Point(243, 186);
            this.txtLugaresEc.MaxLength = 32767;
            this.txtLugaresEc.Name = "txtLugaresEc";
            this.txtLugaresEc.PasswordChar = '\0';
            this.txtLugaresEc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLugaresEc.SelectedText = "";
            this.txtLugaresEc.SelectionLength = 0;
            this.txtLugaresEc.SelectionStart = 0;
            this.txtLugaresEc.ShortcutsEnabled = true;
            this.txtLugaresEc.Size = new System.Drawing.Size(231, 23);
            this.txtLugaresEc.TabIndex = 3;
            this.txtLugaresEc.UseSelectable = true;
            this.txtLugaresEc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLugaresEc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(104, 148);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(107, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Nome Aparelho:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(104, 186);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(130, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Lugares Económicas:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(104, 225);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(120, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "Lugares Executivos:";
            // 
            // bttnGravar
            // 
            this.bttnGravar.Location = new System.Drawing.Point(218, 290);
            this.bttnGravar.Name = "bttnGravar";
            this.bttnGravar.Size = new System.Drawing.Size(164, 59);
            this.bttnGravar.TabIndex = 8;
            this.bttnGravar.Text = "Gravar";
            this.bttnGravar.UseSelectable = true;
            this.bttnGravar.Click += new System.EventHandler(this.bttnGravar_Click);
            // 
            // AparelhosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 454);
            this.Controls.Add(this.bttnGravar);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtLugaresExec);
            this.Controls.Add(this.txtLugaresEc);
            this.Controls.Add(this.txtNomeAparelho);
            this.Name = "AparelhosForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox txtNomeAparelho;
        private MetroFramework.Controls.MetroTextBox txtLugaresExec;
        private MetroFramework.Controls.MetroTextBox txtLugaresEc;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroButton bttnGravar;
    }
}