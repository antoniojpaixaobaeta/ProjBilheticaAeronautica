﻿using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Forms.Reservas
{
    public partial class ReservasMain : MetroFramework.Forms.MetroForm
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();
        Voo voo = new Voo();
        Reserva reserva = new Reserva();
        public ReservasMain()
        {
            InitializeComponent();

            voo = new Voo();
            reserva = new Reserva();
            LoadCombos();
        }

        private void LoadCombos()
        {
            foreach (var item in db.Voos)
            {
                lblShowIDVoo.Items.Add(item.IDAparelho);
            }

            var pesquisa = (from Voo in db.Voos
                            orderby Voo.IDAparelho ascending
                            select Voo.IDVoo).First();

            lblShowIDVoo.Text = pesquisa.ToString();

            foreach (var item in db.Clientes)
            {
                comboEmpresa.Items.Add(item.NomeEmpresa);
            }

            var pesquisaEmpresa = (from Cliente in db.Clientes
                                   orderby Cliente.NomeEmpresa ascending
                                   select Cliente.NomeEmpresa).First();

            comboEmpresa.Text = pesquisaEmpresa.ToString();
        }

        private void LoadVoo()
        {
            Voo procuraVoo = new Voo();

            var pesquisa2 = from Voo in db.Voos
                            where Voo.IDVoo == Convert.ToInt32(lblShowIDVoo.Text)
                            select Voo;

            procuraVoo = pesquisa2.Single();
            
            lblShowOrigem.Text = procuraVoo.Origem;
            lblShowData.Text = procuraVoo.Data.Date.ToString();
            lblShowDestino.Text = procuraVoo.Destino;
            lblShowHoras.Text = procuraVoo.Hora.ToString();
            lblShowHoras.Format = DateTimePickerFormat.Time;
            lblShowHoras.ShowUpDown = true;
            lblShowPrecoEco.Text = Math.Round(procuraVoo.PrecoEconomica, 0).ToString();
            lblShowPrecoExec.Text = Math.Round(procuraVoo.PrecoExecutiva, 0).ToString();
            lblShowEcoDisp.Text = procuraVoo.EconomicaDisponiveis.ToString();
            lblShowExecDisp.Text = procuraVoo.ExecutivaDisponiveis.ToString();
        }

        private void lblShowIDVoo_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadVoo();
        }

        private void bttnReservar_Click(object sender, EventArgs e)
        {
            Cliente descobreCliente = new Cliente();

            var pesquisa = from Cliente in db.Clientes
                           where Cliente.NomeEmpresa == comboEmpresa.Text
                            select Cliente;

            Voo descobreVoo = new Voo();

            var pesquisaVoo = from Voo in db.Voos
                           where Voo.IDVoo == Convert.ToInt32(lblShowIDVoo.Text)
                              select Voo;

            descobreVoo = pesquisaVoo.Single();

            int Exec;
            int Eco;

            if (!((string.IsNullOrEmpty(ExecSelected.Text)) || ((string.IsNullOrEmpty(EcoSelect.Text)))))
            {
                
                if (int.TryParse(ExecSelected.Text, out Exec) && (int.TryParse(EcoSelect.Text, out Eco)))

                {
                    int id = reserva.GeraID();

                    reserva.Create(id, descobreCliente.IDCliente, descobreVoo.IDAparelho, Convert.ToInt32(lblShowIDVoo.Text), Convert.ToInt32(EcoSelect.Text), Convert.ToInt32(ExecSelected.Text));

                    MessageBox.Show("Reserva concluída com sucesso!");

                }

                else
                {
                    MessageBox.Show("Campo numérico inválido. Por favor insira somente números");
                }

            }
            else
            {
                MessageBox.Show("Não pode deixar nenhum campo em branco. Por favor preencha todos os campos.");
            }
        }
    }
}
