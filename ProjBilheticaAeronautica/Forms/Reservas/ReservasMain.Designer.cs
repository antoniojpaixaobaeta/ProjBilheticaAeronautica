﻿namespace ProjBilheticaAeronautica.Forms.Reservas
{
    partial class ReservasMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.comboEmpresa = new MetroFramework.Controls.MetroComboBox();
            this.lblShowIDVoo = new MetroFramework.Controls.MetroComboBox();
            this.lblShowHoras = new System.Windows.Forms.DateTimePicker();
            this.lblShowData = new System.Windows.Forms.DateTimePicker();
            this.lblShowEcoDisp = new MetroFramework.Controls.MetroLabel();
            this.lblShowExecDisp = new MetroFramework.Controls.MetroLabel();
            this.lblShowPrecoEco = new MetroFramework.Controls.MetroTextBox();
            this.lblShowPrecoExec = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.lblShowDestino = new MetroFramework.Controls.MetroTextBox();
            this.lblShowOrigem = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.ExecSelected = new MetroFramework.Controls.MetroTextBox();
            this.EcoSelect = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.bttnReservar = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(33, 91);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(123, 19);
            this.metroLabel1.TabIndex = 55;
            this.metroLabel1.Text = "Nome da Empresa:";
            // 
            // comboEmpresa
            // 
            this.comboEmpresa.FormattingEnabled = true;
            this.comboEmpresa.ItemHeight = 23;
            this.comboEmpresa.Location = new System.Drawing.Point(161, 91);
            this.comboEmpresa.Name = "comboEmpresa";
            this.comboEmpresa.Size = new System.Drawing.Size(160, 29);
            this.comboEmpresa.TabIndex = 57;
            this.comboEmpresa.UseSelectable = true;
            // 
            // lblShowIDVoo
            // 
            this.lblShowIDVoo.FormattingEnabled = true;
            this.lblShowIDVoo.ItemHeight = 23;
            this.lblShowIDVoo.Location = new System.Drawing.Point(161, 213);
            this.lblShowIDVoo.Name = "lblShowIDVoo";
            this.lblShowIDVoo.Size = new System.Drawing.Size(160, 29);
            this.lblShowIDVoo.TabIndex = 134;
            this.lblShowIDVoo.UseSelectable = true;
            this.lblShowIDVoo.SelectedIndexChanged += new System.EventHandler(this.lblShowIDVoo_SelectedIndexChanged);
            // 
            // lblShowHoras
            // 
            this.lblShowHoras.Enabled = false;
            this.lblShowHoras.Location = new System.Drawing.Point(389, 273);
            this.lblShowHoras.Name = "lblShowHoras";
            this.lblShowHoras.Size = new System.Drawing.Size(200, 20);
            this.lblShowHoras.TabIndex = 133;
            // 
            // lblShowData
            // 
            this.lblShowData.Enabled = false;
            this.lblShowData.Location = new System.Drawing.Point(389, 231);
            this.lblShowData.Name = "lblShowData";
            this.lblShowData.Size = new System.Drawing.Size(200, 20);
            this.lblShowData.TabIndex = 132;
            // 
            // lblShowEcoDisp
            // 
            this.lblShowEcoDisp.Location = new System.Drawing.Point(467, 353);
            this.lblShowEcoDisp.Name = "lblShowEcoDisp";
            this.lblShowEcoDisp.Size = new System.Drawing.Size(86, 19);
            this.lblShowEcoDisp.TabIndex = 131;
            this.lblShowEcoDisp.Text = "metroLabel11";
            // 
            // lblShowExecDisp
            // 
            this.lblShowExecDisp.Location = new System.Drawing.Point(472, 316);
            this.lblShowExecDisp.Name = "lblShowExecDisp";
            this.lblShowExecDisp.Size = new System.Drawing.Size(86, 19);
            this.lblShowExecDisp.TabIndex = 117;
            this.lblShowExecDisp.Text = "metroLabel11";
            // 
            // lblShowPrecoEco
            // 
            // 
            // 
            // 
            this.lblShowPrecoEco.CustomButton.Image = null;
            this.lblShowPrecoEco.CustomButton.Location = new System.Drawing.Point(100, 1);
            this.lblShowPrecoEco.CustomButton.Name = "";
            this.lblShowPrecoEco.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowPrecoEco.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowPrecoEco.CustomButton.TabIndex = 1;
            this.lblShowPrecoEco.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowPrecoEco.CustomButton.UseSelectable = true;
            this.lblShowPrecoEco.CustomButton.Visible = false;
            this.lblShowPrecoEco.Enabled = false;
            this.lblShowPrecoEco.Lines = new string[0];
            this.lblShowPrecoEco.Location = new System.Drawing.Point(199, 375);
            this.lblShowPrecoEco.MaxLength = 32767;
            this.lblShowPrecoEco.Name = "lblShowPrecoEco";
            this.lblShowPrecoEco.PasswordChar = '\0';
            this.lblShowPrecoEco.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowPrecoEco.SelectedText = "";
            this.lblShowPrecoEco.SelectionLength = 0;
            this.lblShowPrecoEco.SelectionStart = 0;
            this.lblShowPrecoEco.ShortcutsEnabled = true;
            this.lblShowPrecoEco.Size = new System.Drawing.Size(122, 23);
            this.lblShowPrecoEco.TabIndex = 130;
            this.lblShowPrecoEco.UseSelectable = true;
            this.lblShowPrecoEco.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowPrecoEco.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowPrecoExec
            // 
            // 
            // 
            // 
            this.lblShowPrecoExec.CustomButton.Image = null;
            this.lblShowPrecoExec.CustomButton.Location = new System.Drawing.Point(100, 1);
            this.lblShowPrecoExec.CustomButton.Name = "";
            this.lblShowPrecoExec.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowPrecoExec.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowPrecoExec.CustomButton.TabIndex = 1;
            this.lblShowPrecoExec.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowPrecoExec.CustomButton.UseSelectable = true;
            this.lblShowPrecoExec.CustomButton.Visible = false;
            this.lblShowPrecoExec.Enabled = false;
            this.lblShowPrecoExec.Lines = new string[0];
            this.lblShowPrecoExec.Location = new System.Drawing.Point(199, 335);
            this.lblShowPrecoExec.MaxLength = 32767;
            this.lblShowPrecoExec.Name = "lblShowPrecoExec";
            this.lblShowPrecoExec.PasswordChar = '\0';
            this.lblShowPrecoExec.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowPrecoExec.SelectedText = "";
            this.lblShowPrecoExec.SelectionLength = 0;
            this.lblShowPrecoExec.SelectionStart = 0;
            this.lblShowPrecoExec.ShortcutsEnabled = true;
            this.lblShowPrecoExec.Size = new System.Drawing.Size(122, 23);
            this.lblShowPrecoExec.TabIndex = 129;
            this.lblShowPrecoExec.UseSelectable = true;
            this.lblShowPrecoExec.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowPrecoExec.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(73, 375);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(130, 19);
            this.metroLabel2.TabIndex = 128;
            this.metroLabel2.Text = "Preço C. Económica:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(344, 316);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(122, 19);
            this.metroLabel5.TabIndex = 127;
            this.metroLabel5.Text = "L. Exec. Disponíveis:";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(344, 353);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(117, 19);
            this.metroLabel7.TabIndex = 126;
            this.metroLabel7.Text = "L. Eco. Disponíveis:";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(73, 335);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(119, 19);
            this.metroLabel8.TabIndex = 125;
            this.metroLabel8.Text = "Preço C. Executiva:";
            // 
            // lblShowDestino
            // 
            // 
            // 
            // 
            this.lblShowDestino.CustomButton.Image = null;
            this.lblShowDestino.CustomButton.Location = new System.Drawing.Point(165, 1);
            this.lblShowDestino.CustomButton.Name = "";
            this.lblShowDestino.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowDestino.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowDestino.CustomButton.TabIndex = 1;
            this.lblShowDestino.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowDestino.CustomButton.UseSelectable = true;
            this.lblShowDestino.CustomButton.Visible = false;
            this.lblShowDestino.Enabled = false;
            this.lblShowDestino.Lines = new string[0];
            this.lblShowDestino.Location = new System.Drawing.Point(134, 294);
            this.lblShowDestino.MaxLength = 32767;
            this.lblShowDestino.Name = "lblShowDestino";
            this.lblShowDestino.PasswordChar = '\0';
            this.lblShowDestino.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowDestino.SelectedText = "";
            this.lblShowDestino.SelectionLength = 0;
            this.lblShowDestino.SelectionStart = 0;
            this.lblShowDestino.ShortcutsEnabled = true;
            this.lblShowDestino.Size = new System.Drawing.Size(187, 23);
            this.lblShowDestino.TabIndex = 124;
            this.lblShowDestino.UseSelectable = true;
            this.lblShowDestino.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowDestino.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowOrigem
            // 
            // 
            // 
            // 
            this.lblShowOrigem.CustomButton.Image = null;
            this.lblShowOrigem.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.lblShowOrigem.CustomButton.Name = "";
            this.lblShowOrigem.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowOrigem.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowOrigem.CustomButton.TabIndex = 1;
            this.lblShowOrigem.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowOrigem.CustomButton.UseSelectable = true;
            this.lblShowOrigem.CustomButton.Visible = false;
            this.lblShowOrigem.Enabled = false;
            this.lblShowOrigem.Lines = new string[0];
            this.lblShowOrigem.Location = new System.Drawing.Point(137, 253);
            this.lblShowOrigem.MaxLength = 32767;
            this.lblShowOrigem.Name = "lblShowOrigem";
            this.lblShowOrigem.PasswordChar = '\0';
            this.lblShowOrigem.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowOrigem.SelectedText = "";
            this.lblShowOrigem.SelectionLength = 0;
            this.lblShowOrigem.SelectionStart = 0;
            this.lblShowOrigem.ShortcutsEnabled = true;
            this.lblShowOrigem.Size = new System.Drawing.Size(184, 23);
            this.lblShowOrigem.TabIndex = 123;
            this.lblShowOrigem.UseSelectable = true;
            this.lblShowOrigem.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowOrigem.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(73, 253);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(58, 19);
            this.metroLabel10.TabIndex = 122;
            this.metroLabel10.Text = "Origem:";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(344, 231);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(39, 19);
            this.metroLabel9.TabIndex = 121;
            this.metroLabel9.Text = "Data:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(73, 294);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(55, 19);
            this.metroLabel4.TabIndex = 120;
            this.metroLabel4.Text = "Destino:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(342, 271);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(41, 19);
            this.metroLabel3.TabIndex = 119;
            this.metroLabel3.Text = "Hora:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(73, 213);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(51, 19);
            this.metroLabel6.TabIndex = 118;
            this.metroLabel6.Text = "ID Voo:";
            // 
            // ExecSelected
            // 
            // 
            // 
            // 
            this.ExecSelected.CustomButton.Image = null;
            this.ExecSelected.CustomButton.Location = new System.Drawing.Point(147, 1);
            this.ExecSelected.CustomButton.Name = "";
            this.ExecSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.ExecSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.ExecSelected.CustomButton.TabIndex = 1;
            this.ExecSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.ExecSelected.CustomButton.UseSelectable = true;
            this.ExecSelected.CustomButton.Visible = false;
            this.ExecSelected.Lines = new string[0];
            this.ExecSelected.Location = new System.Drawing.Point(398, 132);
            this.ExecSelected.MaxLength = 32767;
            this.ExecSelected.Name = "ExecSelected";
            this.ExecSelected.PasswordChar = '\0';
            this.ExecSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ExecSelected.SelectedText = "";
            this.ExecSelected.SelectionLength = 0;
            this.ExecSelected.SelectionStart = 0;
            this.ExecSelected.ShortcutsEnabled = true;
            this.ExecSelected.Size = new System.Drawing.Size(169, 23);
            this.ExecSelected.TabIndex = 138;
            this.ExecSelected.UseSelectable = true;
            this.ExecSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.ExecSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // EcoSelect
            // 
            // 
            // 
            // 
            this.EcoSelect.CustomButton.Image = null;
            this.EcoSelect.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.EcoSelect.CustomButton.Name = "";
            this.EcoSelect.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.EcoSelect.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.EcoSelect.CustomButton.TabIndex = 1;
            this.EcoSelect.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.EcoSelect.CustomButton.UseSelectable = true;
            this.EcoSelect.CustomButton.Visible = false;
            this.EcoSelect.Lines = new string[0];
            this.EcoSelect.Location = new System.Drawing.Point(409, 91);
            this.EcoSelect.MaxLength = 32767;
            this.EcoSelect.Name = "EcoSelect";
            this.EcoSelect.PasswordChar = '\0';
            this.EcoSelect.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.EcoSelect.SelectedText = "";
            this.EcoSelect.SelectionLength = 0;
            this.EcoSelect.SelectionStart = 0;
            this.EcoSelect.ShortcutsEnabled = true;
            this.EcoSelect.Size = new System.Drawing.Size(158, 23);
            this.EcoSelect.TabIndex = 137;
            this.EcoSelect.UseSelectable = true;
            this.EcoSelect.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.EcoSelect.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(327, 91);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(76, 19);
            this.metroLabel11.TabIndex = 136;
            this.metroLabel11.Text = "Económica:";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(327, 132);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(65, 19);
            this.metroLabel12.TabIndex = 135;
            this.metroLabel12.Text = "Executiva:";
            // 
            // bttnReservar
            // 
            this.bttnReservar.Location = new System.Drawing.Point(35, 159);
            this.bttnReservar.Name = "bttnReservar";
            this.bttnReservar.Size = new System.Drawing.Size(222, 33);
            this.bttnReservar.TabIndex = 139;
            this.bttnReservar.Text = "Reservar";
            this.bttnReservar.UseSelectable = true;
            this.bttnReservar.Click += new System.EventHandler(this.bttnReservar_Click);
            // 
            // ReservasMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 444);
            this.Controls.Add(this.bttnReservar);
            this.Controls.Add(this.ExecSelected);
            this.Controls.Add(this.EcoSelect);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.lblShowIDVoo);
            this.Controls.Add(this.lblShowHoras);
            this.Controls.Add(this.lblShowData);
            this.Controls.Add(this.lblShowEcoDisp);
            this.Controls.Add(this.lblShowExecDisp);
            this.Controls.Add(this.lblShowPrecoEco);
            this.Controls.Add(this.lblShowPrecoExec);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.lblShowDestino);
            this.Controls.Add(this.lblShowOrigem);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.comboEmpresa);
            this.Controls.Add(this.metroLabel1);
            this.Name = "ReservasMain";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox comboEmpresa;
        private MetroFramework.Controls.MetroComboBox lblShowIDVoo;
        private System.Windows.Forms.DateTimePicker lblShowHoras;
        private System.Windows.Forms.DateTimePicker lblShowData;
        private MetroFramework.Controls.MetroLabel lblShowEcoDisp;
        private MetroFramework.Controls.MetroLabel lblShowExecDisp;
        private MetroFramework.Controls.MetroTextBox lblShowPrecoEco;
        private MetroFramework.Controls.MetroTextBox lblShowPrecoExec;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox lblShowDestino;
        private MetroFramework.Controls.MetroTextBox lblShowOrigem;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox ExecSelected;
        private MetroFramework.Controls.MetroTextBox EcoSelect;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroButton bttnReservar;
    }
}