﻿using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica
{
    public partial class AparelhosForm : MetroFramework.Forms.MetroForm
    {
        Aparelho aparelho = new Aparelho();
        public AparelhosForm()
        {
            InitializeComponent();
        }

        private void bttnGravar_Click(object sender, EventArgs e)
        {
            aparelho = new Aparelho();

            int id = aparelho.GeraID();
            int Eco;
            int Exec;


            if (!((string.IsNullOrEmpty(txtLugaresEc.Text)) || ((string.IsNullOrEmpty(txtLugaresExec.Text)) || ((string.IsNullOrEmpty(txtNomeAparelho.Text))))))
            {

                if (int.TryParse(txtLugaresExec.Text, out Exec) && (int.TryParse(txtLugaresEc.Text, out Eco)))

                {
                    aparelho.Create(id, txtNomeAparelho.Text, Convert.ToInt32(txtLugaresEc.Text), Convert.ToInt32(txtLugaresExec.Text));

                    MessageBox.Show("Aparelho criado com sucesso!");

                }

                else
                {
                    MessageBox.Show("Campo numérico inválido. Por favor insira somente números");
                }

            }
            else
            {
                MessageBox.Show("Não pode deixar nenhum campo em branco. Por favor preencha todos os campos.");
            }
        }
    }
}
