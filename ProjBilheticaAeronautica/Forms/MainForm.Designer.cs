﻿namespace ProjBilheticaAeronautica.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bttnLogin = new MetroFramework.Controls.MetroButton();
            this.lblPass = new MetroFramework.Controls.MetroLabel();
            this.lblUser = new MetroFramework.Controls.MetroLabel();
            this.txtUser = new MetroFramework.Controls.MetroTextBox();
            this.txtPass = new MetroFramework.Controls.MetroTextBox();
            this.bttnClientes = new MetroFramework.Controls.MetroButton();
            this.bttnVoos = new MetroFramework.Controls.MetroButton();
            this.bttnReservas = new MetroFramework.Controls.MetroButton();
            this.bttnAparelhos = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // bttnLogin
            // 
            this.bttnLogin.Location = new System.Drawing.Point(386, 322);
            this.bttnLogin.Name = "bttnLogin";
            this.bttnLogin.Size = new System.Drawing.Size(75, 21);
            this.bttnLogin.TabIndex = 9;
            this.bttnLogin.Text = "Login";
            this.bttnLogin.UseSelectable = true;
            this.bttnLogin.Click += new System.EventHandler(this.bttnLogin_Click);
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(270, 283);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(66, 19);
            this.lblPass.TabIndex = 8;
            this.lblPass.Text = "Password:";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(270, 240);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(71, 19);
            this.lblUser.TabIndex = 7;
            this.lblUser.Text = "Username:";
            // 
            // txtUser
            // 
            // 
            // 
            // 
            this.txtUser.CustomButton.Image = null;
            this.txtUser.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.txtUser.CustomButton.Name = "";
            this.txtUser.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUser.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUser.CustomButton.TabIndex = 1;
            this.txtUser.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUser.CustomButton.UseSelectable = true;
            this.txtUser.CustomButton.Visible = false;
            this.txtUser.Lines = new string[0];
            this.txtUser.Location = new System.Drawing.Point(342, 238);
            this.txtUser.MaxLength = 32767;
            this.txtUser.Name = "txtUser";
            this.txtUser.PasswordChar = '\0';
            this.txtUser.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUser.SelectedText = "";
            this.txtUser.SelectionLength = 0;
            this.txtUser.SelectionStart = 0;
            this.txtUser.ShortcutsEnabled = true;
            this.txtUser.Size = new System.Drawing.Size(191, 23);
            this.txtUser.TabIndex = 6;
            this.txtUser.UseSelectable = true;
            this.txtUser.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUser.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPass
            // 
            // 
            // 
            // 
            this.txtPass.CustomButton.Image = null;
            this.txtPass.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.txtPass.CustomButton.Name = "";
            this.txtPass.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPass.CustomButton.TabIndex = 1;
            this.txtPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPass.CustomButton.UseSelectable = true;
            this.txtPass.CustomButton.Visible = false;
            this.txtPass.Lines = new string[0];
            this.txtPass.Location = new System.Drawing.Point(342, 281);
            this.txtPass.MaxLength = 32767;
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '\0';
            this.txtPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPass.SelectedText = "";
            this.txtPass.SelectionLength = 0;
            this.txtPass.SelectionStart = 0;
            this.txtPass.ShortcutsEnabled = true;
            this.txtPass.Size = new System.Drawing.Size(191, 23);
            this.txtPass.TabIndex = 5;
            this.txtPass.UseSelectable = true;
            this.txtPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // bttnClientes
            // 
            this.bttnClientes.Location = new System.Drawing.Point(149, 72);
            this.bttnClientes.Name = "bttnClientes";
            this.bttnClientes.Size = new System.Drawing.Size(126, 123);
            this.bttnClientes.TabIndex = 10;
            this.bttnClientes.Text = "Clientes";
            this.bttnClientes.UseSelectable = true;
            this.bttnClientes.Click += new System.EventHandler(this.bttnClientes_Click);
            // 
            // bttnVoos
            // 
            this.bttnVoos.Location = new System.Drawing.Point(270, 72);
            this.bttnVoos.Name = "bttnVoos";
            this.bttnVoos.Size = new System.Drawing.Size(126, 123);
            this.bttnVoos.TabIndex = 11;
            this.bttnVoos.Text = "Voos";
            this.bttnVoos.UseSelectable = true;
            this.bttnVoos.Click += new System.EventHandler(this.bttnVoos_Click);
            // 
            // bttnReservas
            // 
            this.bttnReservas.Location = new System.Drawing.Point(393, 72);
            this.bttnReservas.Name = "bttnReservas";
            this.bttnReservas.Size = new System.Drawing.Size(126, 123);
            this.bttnReservas.TabIndex = 12;
            this.bttnReservas.Text = "Reservas";
            this.bttnReservas.UseSelectable = true;
            this.bttnReservas.Click += new System.EventHandler(this.bttnReservas_Click);
            // 
            // bttnAparelhos
            // 
            this.bttnAparelhos.Location = new System.Drawing.Point(516, 72);
            this.bttnAparelhos.Name = "bttnAparelhos";
            this.bttnAparelhos.Size = new System.Drawing.Size(126, 123);
            this.bttnAparelhos.TabIndex = 13;
            this.bttnAparelhos.Text = "Aparelhos";
            this.bttnAparelhos.UseSelectable = true;
            this.bttnAparelhos.Click += new System.EventHandler(this.bttnAparelhos_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 380);
            this.Controls.Add(this.bttnAparelhos);
            this.Controls.Add(this.bttnReservas);
            this.Controls.Add(this.bttnVoos);
            this.Controls.Add(this.bttnClientes);
            this.Controls.Add(this.bttnLogin);
            this.Controls.Add(this.lblPass);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.txtPass);
            this.Name = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton bttnLogin;
        private MetroFramework.Controls.MetroLabel lblPass;
        private MetroFramework.Controls.MetroLabel lblUser;
        private MetroFramework.Controls.MetroTextBox txtUser;
        private MetroFramework.Controls.MetroTextBox txtPass;
        private MetroFramework.Controls.MetroButton bttnClientes;
        private MetroFramework.Controls.MetroButton bttnVoos;
        private MetroFramework.Controls.MetroButton bttnReservas;
        private MetroFramework.Controls.MetroButton bttnAparelhos;
    }
}