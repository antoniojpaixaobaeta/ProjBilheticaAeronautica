﻿using ProjBilheticaAeronautica.Forms.Reservas;
using ProjBilheticaAeronautica.Forms.Voos;
using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Forms
{
    public partial class MainForm : MetroFramework.Forms.MetroForm
    {
        Utilizadore utilizador = new Utilizadore();
        ClientesMainForm clientesMainForm = new ClientesMainForm();
        VoosMainForm voosMainForm = new VoosMainForm();
        ReservasMain reservasMain = new ReservasMain();
        AparelhosForm aparelhosForm = new AparelhosForm();
        
        public MainForm()
        {
            InitializeComponent();
            bttnAparelhos.Visible = false;
            bttnReservas.Visible = false;
            bttnVoos.Visible = false;
            bttnClientes.Visible = false;
        }

        private void bttnLogin_Click(object sender, EventArgs e)
        {
            var val = utilizador.Login(txtUser.Text, txtPass.Text);

            if (val == true)
            {
                txtPass.Hide();
                txtUser.Hide();
                lblPass.Hide();
                lblUser.Hide();
                bttnLogin.Hide();

                bttnAparelhos.Visible = true;
                bttnReservas.Visible = true;
                bttnVoos.Visible = true;
                bttnClientes.Visible = true;
            }
            else
            {
                txtPass.Clear();
                txtUser.Clear();
            }


        }

        private void bttnClientes_Click(object sender, EventArgs e)
        {
            clientesMainForm = new ClientesMainForm();

            clientesMainForm.Show();
        }

        private void bttnVoos_Click(object sender, EventArgs e)
        {
            voosMainForm = new VoosMainForm();

            voosMainForm.Show();
        }

        private void bttnReservas_Click(object sender, EventArgs e)
        {
            reservasMain = new ReservasMain();
            reservasMain.Show();
        }

        private void bttnAparelhos_Click(object sender, EventArgs e)
        {
            aparelhosForm = new AparelhosForm();
            aparelhosForm.Show();
        }
    }
}
