﻿using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Forms.Voos
{
    public partial class VoosMainForm : MetroFramework.Forms.MetroForm
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();
        Voo voo = new Voo();
        VoosNovo voosNovo = new VoosNovo();

        public VoosMainForm()
        {
            InitializeComponent();
            LoadComboID();
            LoadLabels();
        }

        private void LoadLabels()
        {
            voo = new Voo();

            voo = voo.Read(Convert.ToInt32(comboID.Text));

            lblShowIDAparelho.Text = voo.IDAparelho.ToString();
            lblShowOrigem.Text = voo.Origem;
            lblShowData.Text = voo.Data.Date.ToString();
            lblShowDestino.Text = voo.Destino;
            lblShowHoras.Text = voo.Hora.ToString();
            lblShowHoras.Format = DateTimePickerFormat.Time;
            lblShowHoras.ShowUpDown = true;
            lblShowPrecoEco.Text = Math.Round(voo.PrecoEconomica, 0).ToString();
            lblShowPrecoExec.Text = Math.Round(voo.PrecoExecutiva, 0).ToString();
            lblShowEcoDisp.Text = voo.EconomicaDisponiveis.ToString();
            lblShowExecDisp.Text = voo.ExecutivaDisponiveis.ToString();
        }

        private void LoadComboID()
        {
            foreach (var item in db.Voos)
            {
                comboID.Items.Add(item.IDVoo);
            }

            var pesquisa = (from Voo in db.Voos
                            orderby Voo.IDVoo ascending
                            select Voo.IDVoo).First();

            comboID.Text = pesquisa.ToString();
        }

        private void comboID_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLabels();
        }

        private void bttnApagar_Click(object sender, EventArgs e)
        {
            bool ex = voo.Delete(Convert.ToInt32(comboID.Text));

            if (ex == false)
            {
                MessageBox.Show("Voo apagado com sucesso!");
                comboID.Items.Clear();
                LoadComboID();
            }
            else
            {
                return;
            }
        }

        private void bttnEditar_Click(object sender, EventArgs e)
        {
            int precoEco;
            int precoExe;
               

            if (!((string.IsNullOrEmpty(lblShowOrigem.Text)) || ((string.IsNullOrEmpty(lblShowDestino.Text)) || ((string.IsNullOrEmpty(lblShowPrecoEco.Text)) ||
                (string.IsNullOrEmpty(lblShowPrecoExec.Text))))))
            {
                if (int.TryParse(lblShowPrecoExec.Text, out precoEco) && (int.TryParse(lblShowPrecoEco.Text, out precoExe)))

                {
                    voo.Update(Convert.ToInt32(comboID.Text), Convert.ToInt32(lblShowPrecoEco.Text), Convert.ToInt32(lblShowPrecoExec.Text),
                        lblShowOrigem.Text, lblShowDestino.Text, lblShowData.Value.Date, lblShowHoras.Text);

                    MessageBox.Show("Voo editado com sucesso!");

                }

                else
                {
                    MessageBox.Show("Campo numérico inválido. Por favor insira somente números");
                }
            }
            else
            {
                MessageBox.Show("Não pode deixar nenhum campo em branco. Por favor preencha todos os campos.");
            }
        }

        private void bttnNovo_Click(object sender, EventArgs e)
        {
            voosNovo = new VoosNovo();
            voosNovo.Show();
        }
    }
}
