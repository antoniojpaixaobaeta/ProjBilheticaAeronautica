﻿using ProjBilheticaAeronautica.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjBilheticaAeronautica.Forms.Voos
{
    public partial class VoosNovo : MetroFramework.Forms.MetroForm
    {
        DataClassesDesktopAppDataContext db = new DataClassesDesktopAppDataContext();
        Voo voo = new Voo();
        public VoosNovo()
        {
            InitializeComponent();
            voo = new Voo();
            LoadComboID();
            lblShowHoras.Format = DateTimePickerFormat.Time;
            lblShowHoras.ShowUpDown = true;

        }

        private void LoadComboID()
        {
            foreach (var item in db.Aparelhos)
            {
                lblShowIDAparelho.Items.Add(item.IDAparelho);
            }

            var pesquisa = (from Aparelho in db.Aparelhos
                            orderby Aparelho.IDAparelho ascending
                            select Aparelho.IDAparelho).First();

            lblShowIDAparelho.Text = pesquisa.ToString();

            LoadLugares();

        }

        private void LoadLugares()
        {
            Aparelho procuraVoo = new Aparelho();

            var pesquisa2 = from Aparelho in db.Aparelhos
                            where Aparelho.IDAparelho == Convert.ToInt32(lblShowIDAparelho.Text)
                            select Aparelho;

            procuraVoo = pesquisa2.Single();


            lblShowEcoDisp.Text = procuraVoo.LugaresEconomica.ToString();
            lblShowExecDisp.Text = procuraVoo.LugaresExecutiva.ToString();
        }

        private void lblShowIDAparelho_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLugares();
        }

        private void bttnGravar_Click(object sender, EventArgs e)
        {
            int id = voo.GeraID();
            int precoEco;
            int precoExe;


            if (!((string.IsNullOrEmpty(lblShowOrigem.Text)) || ((string.IsNullOrEmpty(lblShowDestino.Text)) || ((string.IsNullOrEmpty(lblShowPrecoEco.Text)) ||
                (string.IsNullOrEmpty(lblShowPrecoExec.Text))))))
            {
                if (int.TryParse(lblShowPrecoExec.Text, out precoEco) && (int.TryParse(lblShowPrecoEco.Text, out precoExe)))

                {
                    voo.Create(id, Convert.ToInt32(lblShowIDAparelho.Text), precoEco, precoExe,
                        lblShowOrigem.Text, lblShowDestino.Text, lblShowData.Value.Date, lblShowHoras.Text, Convert.ToInt32(lblShowExecDisp.Text), Convert.ToInt32(lblShowEcoDisp.Text));

                    MessageBox.Show("Voo gravado com sucesso!");

                }

                else
                {
                    MessageBox.Show("Campo numérico inválido. Por favor insira somente números");
                }
            }
            else
            {
                MessageBox.Show("Não pode deixar nenhum campo em branco. Por favor preencha todos os campos.");
            }
        }
    }
}
