﻿namespace ProjBilheticaAeronautica.Forms.Voos
{
    partial class VoosMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.lblShowPrecoEco = new MetroFramework.Controls.MetroTextBox();
            this.lblShowPrecoExec = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.lblShowDestino = new MetroFramework.Controls.MetroTextBox();
            this.lblShowOrigem = new MetroFramework.Controls.MetroTextBox();
            this.bttnApagar = new MetroFramework.Controls.MetroButton();
            this.bttnEditar = new MetroFramework.Controls.MetroButton();
            this.bttnNovo = new MetroFramework.Controls.MetroButton();
            this.comboID = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblShowIDAparelho = new MetroFramework.Controls.MetroLabel();
            this.lblShowExecDisp = new MetroFramework.Controls.MetroLabel();
            this.lblShowEcoDisp = new MetroFramework.Controls.MetroLabel();
            this.lblShowData = new System.Windows.Forms.DateTimePicker();
            this.lblShowHoras = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(337, 238);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(51, 19);
            this.metroLabel6.TabIndex = 91;
            this.metroLabel6.Text = "ID Voo:";
            // 
            // lblShowPrecoEco
            // 
            // 
            // 
            // 
            this.lblShowPrecoEco.CustomButton.Image = null;
            this.lblShowPrecoEco.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowPrecoEco.CustomButton.Name = "";
            this.lblShowPrecoEco.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowPrecoEco.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowPrecoEco.CustomButton.TabIndex = 1;
            this.lblShowPrecoEco.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowPrecoEco.CustomButton.UseSelectable = true;
            this.lblShowPrecoEco.CustomButton.Visible = false;
            this.lblShowPrecoEco.Lines = new string[0];
            this.lblShowPrecoEco.Location = new System.Drawing.Point(231, 238);
            this.lblShowPrecoEco.MaxLength = 32767;
            this.lblShowPrecoEco.Name = "lblShowPrecoEco";
            this.lblShowPrecoEco.PasswordChar = '\0';
            this.lblShowPrecoEco.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowPrecoEco.SelectedText = "";
            this.lblShowPrecoEco.SelectionLength = 0;
            this.lblShowPrecoEco.SelectionStart = 0;
            this.lblShowPrecoEco.ShortcutsEnabled = true;
            this.lblShowPrecoEco.Size = new System.Drawing.Size(96, 23);
            this.lblShowPrecoEco.TabIndex = 88;
            this.lblShowPrecoEco.UseSelectable = true;
            this.lblShowPrecoEco.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowPrecoEco.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowPrecoExec
            // 
            // 
            // 
            // 
            this.lblShowPrecoExec.CustomButton.Image = null;
            this.lblShowPrecoExec.CustomButton.Location = new System.Drawing.Point(74, 1);
            this.lblShowPrecoExec.CustomButton.Name = "";
            this.lblShowPrecoExec.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowPrecoExec.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowPrecoExec.CustomButton.TabIndex = 1;
            this.lblShowPrecoExec.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowPrecoExec.CustomButton.UseSelectable = true;
            this.lblShowPrecoExec.CustomButton.Visible = false;
            this.lblShowPrecoExec.Lines = new string[0];
            this.lblShowPrecoExec.Location = new System.Drawing.Point(231, 198);
            this.lblShowPrecoExec.MaxLength = 32767;
            this.lblShowPrecoExec.Name = "lblShowPrecoExec";
            this.lblShowPrecoExec.PasswordChar = '\0';
            this.lblShowPrecoExec.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowPrecoExec.SelectedText = "";
            this.lblShowPrecoExec.SelectionLength = 0;
            this.lblShowPrecoExec.SelectionStart = 0;
            this.lblShowPrecoExec.ShortcutsEnabled = true;
            this.lblShowPrecoExec.Size = new System.Drawing.Size(96, 23);
            this.lblShowPrecoExec.TabIndex = 87;
            this.lblShowPrecoExec.UseSelectable = true;
            this.lblShowPrecoExec.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowPrecoExec.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(105, 238);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(130, 19);
            this.metroLabel2.TabIndex = 86;
            this.metroLabel2.Text = "Preço C. Económica:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(337, 161);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(122, 19);
            this.metroLabel5.TabIndex = 85;
            this.metroLabel5.Text = "L. Exec. Disponíveis:";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(337, 198);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(117, 19);
            this.metroLabel7.TabIndex = 84;
            this.metroLabel7.Text = "L. Eco. Disponíveis:";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(105, 198);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(119, 19);
            this.metroLabel8.TabIndex = 83;
            this.metroLabel8.Text = "Preço C. Executiva:";
            // 
            // lblShowDestino
            // 
            // 
            // 
            // 
            this.lblShowDestino.CustomButton.Image = null;
            this.lblShowDestino.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.lblShowDestino.CustomButton.Name = "";
            this.lblShowDestino.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowDestino.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowDestino.CustomButton.TabIndex = 1;
            this.lblShowDestino.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowDestino.CustomButton.UseSelectable = true;
            this.lblShowDestino.CustomButton.Visible = false;
            this.lblShowDestino.Lines = new string[0];
            this.lblShowDestino.Location = new System.Drawing.Point(166, 157);
            this.lblShowDestino.MaxLength = 32767;
            this.lblShowDestino.Name = "lblShowDestino";
            this.lblShowDestino.PasswordChar = '\0';
            this.lblShowDestino.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowDestino.SelectedText = "";
            this.lblShowDestino.SelectionLength = 0;
            this.lblShowDestino.SelectionStart = 0;
            this.lblShowDestino.ShortcutsEnabled = true;
            this.lblShowDestino.Size = new System.Drawing.Size(161, 23);
            this.lblShowDestino.TabIndex = 80;
            this.lblShowDestino.UseSelectable = true;
            this.lblShowDestino.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowDestino.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblShowOrigem
            // 
            // 
            // 
            // 
            this.lblShowOrigem.CustomButton.Image = null;
            this.lblShowOrigem.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.lblShowOrigem.CustomButton.Name = "";
            this.lblShowOrigem.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lblShowOrigem.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lblShowOrigem.CustomButton.TabIndex = 1;
            this.lblShowOrigem.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lblShowOrigem.CustomButton.UseSelectable = true;
            this.lblShowOrigem.CustomButton.Visible = false;
            this.lblShowOrigem.Lines = new string[0];
            this.lblShowOrigem.Location = new System.Drawing.Point(169, 116);
            this.lblShowOrigem.MaxLength = 32767;
            this.lblShowOrigem.Name = "lblShowOrigem";
            this.lblShowOrigem.PasswordChar = '\0';
            this.lblShowOrigem.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lblShowOrigem.SelectedText = "";
            this.lblShowOrigem.SelectionLength = 0;
            this.lblShowOrigem.SelectionStart = 0;
            this.lblShowOrigem.ShortcutsEnabled = true;
            this.lblShowOrigem.Size = new System.Drawing.Size(158, 23);
            this.lblShowOrigem.TabIndex = 79;
            this.lblShowOrigem.UseSelectable = true;
            this.lblShowOrigem.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lblShowOrigem.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // bttnApagar
            // 
            this.bttnApagar.Location = new System.Drawing.Point(399, 341);
            this.bttnApagar.Name = "bttnApagar";
            this.bttnApagar.Size = new System.Drawing.Size(97, 78);
            this.bttnApagar.TabIndex = 77;
            this.bttnApagar.Text = "Apagar";
            this.bttnApagar.UseSelectable = true;
            this.bttnApagar.Click += new System.EventHandler(this.bttnApagar_Click);
            // 
            // bttnEditar
            // 
            this.bttnEditar.Location = new System.Drawing.Point(305, 341);
            this.bttnEditar.Name = "bttnEditar";
            this.bttnEditar.Size = new System.Drawing.Size(97, 78);
            this.bttnEditar.TabIndex = 76;
            this.bttnEditar.Text = "Editar";
            this.bttnEditar.UseSelectable = true;
            this.bttnEditar.Click += new System.EventHandler(this.bttnEditar_Click);
            // 
            // bttnNovo
            // 
            this.bttnNovo.Location = new System.Drawing.Point(211, 341);
            this.bttnNovo.Name = "bttnNovo";
            this.bttnNovo.Size = new System.Drawing.Size(97, 78);
            this.bttnNovo.TabIndex = 75;
            this.bttnNovo.Text = "Novo";
            this.bttnNovo.UseSelectable = true;
            this.bttnNovo.Click += new System.EventHandler(this.bttnNovo_Click);
            // 
            // comboID
            // 
            this.comboID.FormattingEnabled = true;
            this.comboID.ItemHeight = 23;
            this.comboID.Location = new System.Drawing.Point(394, 238);
            this.comboID.Name = "comboID";
            this.comboID.Size = new System.Drawing.Size(192, 29);
            this.comboID.TabIndex = 74;
            this.comboID.UseSelectable = true;
            this.comboID.SelectedIndexChanged += new System.EventHandler(this.comboID_SelectedIndexChanged);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(105, 116);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(58, 19);
            this.metroLabel10.TabIndex = 73;
            this.metroLabel10.Text = "Origem:";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(337, 76);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(39, 19);
            this.metroLabel9.TabIndex = 72;
            this.metroLabel9.Text = "Data:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(105, 157);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(55, 19);
            this.metroLabel4.TabIndex = 71;
            this.metroLabel4.Text = "Destino:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(335, 116);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(41, 19);
            this.metroLabel3.TabIndex = 70;
            this.metroLabel3.Text = "Hora:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(105, 76);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(82, 19);
            this.metroLabel1.TabIndex = 69;
            this.metroLabel1.Text = "ID Aparelho:";
            // 
            // lblShowIDAparelho
            // 
            this.lblShowIDAparelho.AutoSize = true;
            this.lblShowIDAparelho.Location = new System.Drawing.Point(193, 76);
            this.lblShowIDAparelho.Name = "lblShowIDAparelho";
            this.lblShowIDAparelho.Size = new System.Drawing.Size(0, 0);
            this.lblShowIDAparelho.TabIndex = 92;
            // 
            // lblShowExecDisp
            // 
            this.lblShowExecDisp.Location = new System.Drawing.Point(465, 161);
            this.lblShowExecDisp.Name = "lblShowExecDisp";
            this.lblShowExecDisp.Size = new System.Drawing.Size(86, 19);
            this.lblShowExecDisp.TabIndex = 0;
            this.lblShowExecDisp.Text = "metroLabel11";
            // 
            // lblShowEcoDisp
            // 
            this.lblShowEcoDisp.Location = new System.Drawing.Point(460, 198);
            this.lblShowEcoDisp.Name = "lblShowEcoDisp";
            this.lblShowEcoDisp.Size = new System.Drawing.Size(86, 19);
            this.lblShowEcoDisp.TabIndex = 93;
            this.lblShowEcoDisp.Text = "metroLabel11";
            // 
            // lblShowData
            // 
            this.lblShowData.Location = new System.Drawing.Point(382, 76);
            this.lblShowData.Name = "lblShowData";
            this.lblShowData.Size = new System.Drawing.Size(200, 20);
            this.lblShowData.TabIndex = 94;
            // 
            // lblShowHoras
            // 
            this.lblShowHoras.Location = new System.Drawing.Point(382, 118);
            this.lblShowHoras.Name = "lblShowHoras";
            this.lblShowHoras.Size = new System.Drawing.Size(200, 20);
            this.lblShowHoras.TabIndex = 95;
            // 
            // VoosMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 480);
            this.Controls.Add(this.lblShowHoras);
            this.Controls.Add(this.lblShowData);
            this.Controls.Add(this.lblShowEcoDisp);
            this.Controls.Add(this.lblShowExecDisp);
            this.Controls.Add(this.lblShowIDAparelho);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.lblShowPrecoEco);
            this.Controls.Add(this.lblShowPrecoExec);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.lblShowDestino);
            this.Controls.Add(this.lblShowOrigem);
            this.Controls.Add(this.bttnApagar);
            this.Controls.Add(this.bttnEditar);
            this.Controls.Add(this.bttnNovo);
            this.Controls.Add(this.comboID);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel1);
            this.Name = "VoosMainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox lblShowPrecoEco;
        private MetroFramework.Controls.MetroTextBox lblShowPrecoExec;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox lblShowDestino;
        private MetroFramework.Controls.MetroTextBox lblShowOrigem;
        private MetroFramework.Controls.MetroButton bttnApagar;
        private MetroFramework.Controls.MetroButton bttnEditar;
        private MetroFramework.Controls.MetroButton bttnNovo;
        private MetroFramework.Controls.MetroComboBox comboID;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblShowIDAparelho;
        private MetroFramework.Controls.MetroLabel lblShowExecDisp;
        private MetroFramework.Controls.MetroLabel lblShowEcoDisp;
        private System.Windows.Forms.DateTimePicker lblShowData;
        private System.Windows.Forms.DateTimePicker lblShowHoras;
    }
}