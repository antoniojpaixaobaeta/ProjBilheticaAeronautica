﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjBilheticaAeronautica.Modelos;

namespace ProjBilheticaAeronautica.Forms
{
    public partial class LoginForm : Form
    {
        Utilizadore utilizador = new Utilizadore();

        public LoginForm()
        {
            InitializeComponent();
        }

        private void bttnLogin_Click(object sender, EventArgs e)
        {
            var val = utilizador.Login(txtUser.Text, txtPass.Text);

            if (val == true)
            {
                Hide();
            }
            else
            {
                txtPass.Clear();
                txtUser.Clear();
            }
        }
    }
}
