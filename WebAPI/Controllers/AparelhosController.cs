﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class AparelhosController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        // GET: api/Aparelhos
        public List<Aparelho> Get()
        {
            var lista = from Aparelho in dc.Aparelhos select Aparelho;

            return lista.ToList();
        }

        // GET: api/Aparelhos/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Aparelhos
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Aparelhos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Aparelhos/5
        public void Delete(int id)
        {
        }
    }
}
