﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class VoosController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: api/Voos
        public List<Voo> Get()
        {
            var lista = from Voo in dc.Voos select Voo;

            return lista.ToList();
        }

        // GET: api/Voos/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Voos
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Voos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Voos/5
        public void Delete(int id)
        {
        }
    }
}
